﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LoadScenes : MonoBehaviour
{
    public void viewScene()
    {
        SceneManager.LoadScene(2);
    }
    public void interactiveScene()
    {
        SceneManager.LoadScene(1);

    }
    public void dancingScene()
    {
        SceneManager.LoadScene(3);
    }
    public void medianScene()
    {
        SceneManager.LoadScene(0);
    }
    public void gameExit()
    {
        Application.Quit();
    }
}
