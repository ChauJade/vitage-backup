﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;


public class Try : MonoBehaviour
{
    public GameObject patternObject;
    public GameObject[] enableObjectArr;
    public VRTK_Pointer Pointer;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Pointer.IsSelectionButtonPressed())
        {
            foreach (GameObject enemy in enableObjectArr)
            {
                enemy.SetActive(true);
            }
        }
        
    }
}
