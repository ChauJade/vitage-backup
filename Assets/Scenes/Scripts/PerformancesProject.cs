using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class PerformancesProject : MonoBehaviour
{
    [Header("SCENE 1")]
    public Image sceneOneBackground;
    public Image trongDong;
    public Image VietNamMap;
    public Image logo;
    public TMP_Text description;
    //public BackgroundHolder backgroundHolder;

    [Header("SCENE 2")]
    public Image[] sceneTwoBackground;
    public Image logoPNG;
    public Image HMILogo;

    public Button changeEnglish;
    public Button changeVN;
    public AudioSource audioTwo;

    [Header("GATE")]
    public GameObject _gate1;
    public GameObject _gate2;
    public GameObject _gate3;

    [Header("OTHER")]
    public float sceneOneTime = 1f;
    public GameObject _user;
    [SerializeField] Vector3 vector;

    [Header("REMAKE SCENE 2")]
    public Sprite[] _sprites = new Sprite[3];
    public Image _image;
    [SerializeField] float _period;
    [SerializeField] AudioSource _audio;




    
    // Start is called before the first frame update
    void Start()
    {
        SceneOne();
        audioTwo.Stop();
        _audio.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        //StartCoroutine(ChangeScene());

        StartCoroutine("ChangeSceneTwo");
    }
    IEnumerator ChangeScene()
    {
        yield return new WaitForSeconds(11f); //11
        SceneTwo();
    }

    void SceneOne()
    {
        _image.enabled = false;
        VietNamMap.enabled = true;
        logoPNG.enabled = false;
        trongDong.enabled = true;
        HMILogo.enabled = false;
        /*
        room1.gameObject.SetActive(false);
        room2.gameObject.SetActive(false);
        room3.gameObject.SetActive(false);
        */
        _gate1.SetActive(false);
        _gate2.SetActive(false);
        _gate3.SetActive(false);

        changeEnglish.gameObject.SetActive(false);
        changeVN.gameObject.SetActive(false);
        audioTwo.enabled = false;
        description.gameObject.SetActive(true);

        for (int i = 0; i < sceneTwoBackground.Length; i++)
        {
            sceneTwoBackground[i].enabled = false;
        }

        AudioManager.instance.Play("DongMauLacHong");
        Sequence sequence = DOTween.Sequence();
        sequence/*.Append(logo.DOBlendableColor(Color.white, 1f))*/
                .Append(logo.transform.DOScale(2, 4f))
                .Append(logo.transform.DOScale(1, 4f))
                .Append(logo.DOFade(0, sceneOneTime))
                .Join(description.DOFade(0, sceneOneTime))
                .Join(VietNamMap.DOFade(0, sceneOneTime))
                .Join(sceneOneBackground.DOFade(0, sceneOneTime))
                .Join(trongDong.DOFade(0, sceneOneTime));
                /*.Join(curBackground.DOFade(0, sceneOneTime - 1))*/;
    }

    void SceneTwo()
    {
        
        Sequence sequence1 = DOTween.Sequence();

        audioTwo.enabled = true;
        audioTwo.DOFade(1, 33f).OnComplete(() =>
        {
            changeEnglish.gameObject.SetActive(true);
            changeVN.gameObject.SetActive(true);
            _gate1.SetActive(true);
            _gate2.SetActive(true);
            _gate3.SetActive(true);
            _user.GetComponent<Transform>().position = vector;
  
        });
        Sequence sequence = DOTween.Sequence();
        VietNamMap.enabled = false;
        trongDong.enabled = false;
        description.gameObject.SetActive(false);
        sceneOneBackground.gameObject.SetActive(false);
        //AudioManager.instance.Play("IntroEnglish");
        sceneTwoBackground[0].enabled = true;
        sequence.Append(sceneTwoBackground[0].DOFade(0, 2f).OnComplete(() =>
        {
            sceneTwoBackground[0].enabled = false;
            if(Time.frameCount == 5f)
            {
                sceneTwoBackground[2].enabled = true;
            }
            //sceneTwoBackground[1].enabled = true;
            /*sceneTwoBackground[1].DOFade(0, 5f).OnComplete(() =>
            {
                sceneTwoBackground[1].enabled = false;
                sceneTwoBackground[2].enabled = true;
                sceneTwoBackground[2].DOFade(0, 5f).OnComplete(() =>
                {
                    sceneTwoBackground[2].enabled = false;
                    sceneTwoBackground[3].enabled = true;

                });

            });*/
            
        })).Append(sceneTwoBackground[1].DOFade(0, 2f).OnComplete(() =>
        {
            sceneTwoBackground[1].enabled = false;
            if(Time.frameCount == 10f)
            {
                sceneTwoBackground[2].enabled = true;
            }
            //sceneTwoBackground[2].enabled = true;
            

        })).Append(sceneTwoBackground[2].DOFade(0, 2f).OnComplete(() =>
        {
            sceneTwoBackground[2].enabled = false;
            sceneTwoBackground[3].enabled = true;

        }));
        
        logoPNG.enabled = true;
        HMILogo.enabled = true;
        /*
        room1.gameObject.SetActive(true);
        room2.gameObject.SetActive(true);
        room3.gameObject.SetActive(true);
        */

        /*
        _gate1.SetActive(true);
        _gate2.SetActive(true);
        _gate3.SetActive(true);
        */
    }

    public IEnumerator ChangeSceneTwo()
    {
        yield return new WaitForSeconds(11f);
        _audio.enabled = true;
        while(_audio.isPlaying)
        {
            _image.sprite = _sprites[Random.Range(0, _sprites .Length)];
            _image.enabled = true;
            yield return new WaitForSeconds(2f);
            _image.enabled = false;
            yield return new WaitForSeconds(2f);

        //     image.sprite = sprites[Random.Range(0, sprites .Length)];
        //    image.enabled = true;
        //    yield return new WaitForSeconds(1);
        //   image.enabled = false;
        }
        StopCoroutine("ChangeSceneTwo");
        Debug.Log("Audio finished");
    }
}
