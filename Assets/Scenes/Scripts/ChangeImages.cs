﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;

public class ChangeImages : MonoBehaviour
{
    [Header("TIME")]
    [Tooltip("Thoi gian giua 2 lan chuyen hinh anh")]
    public float _period = 2f;
    //public float _changeImageTime;

    [Header("IMAGE")]
    public Image _image;
    public Sprite[] _sprites;


    [Header("VIDEO & AUDIO")]
    public RawImage _video;
    public AudioSource _audio;

    [Header("LOGO")]
    public Image logoPNG;
    public Image HMILogo;

    [Header("GATE")]
    public GameObject _gate1;
    public GameObject _gate2;
    public GameObject _gate3;

    [Header("USER POSITION")]
    [SerializeField] GameObject _user;
    [SerializeField] Vector3 vector;
    // Start is called before the first frame update
    void Start()
    {
        _audio.enabled = false;
        _image.enabled = false;
        _video.enabled = false;
        _video.GetComponent<VideoPlayer>().enabled = false;

        logoPNG.enabled = false;
        HMILogo.enabled = false;

        _gate1.SetActive(false);
        _gate2.SetActive(false);
        _gate3.SetActive(false);

        StartCoroutine(methodName: "ChangeImage");
    }

    public IEnumerator ChangeImage()
    {
        yield return new WaitForSeconds(11f);
        _audio.enabled = true;
        _video.enabled = false;
        _video.GetComponent<VideoPlayer>().enabled = false;

        logoPNG.enabled = true;
        HMILogo.enabled = true;
        while(_audio.isPlaying)
        {
            _gate1.SetActive(false);
            _gate2.SetActive(false);
            _gate3.SetActive(false);
            logoPNG.enabled = true;
            HMILogo.enabled = true;

            _video.enabled = false;
            _video.GetComponent<VideoPlayer>().enabled = false;
            _image.sprite = _sprites[Random.Range(0, _sprites .Length)];
            _image.enabled = true;
            yield return new WaitForSeconds(_period);
            _image.enabled = false;

        }
        StopCoroutine("ChangeImage");
        Debug.Log("Audio finished");
        
        logoPNG.enabled = false;
        HMILogo.enabled = false;
        _gate1.SetActive(true);
        _gate2.SetActive(true);
        _gate3.SetActive(true);
        _user.GetComponent<Transform>().position = vector;
        _video.enabled = true;
        _video.GetComponent<VideoPlayer>().enabled = true;

    }
}
