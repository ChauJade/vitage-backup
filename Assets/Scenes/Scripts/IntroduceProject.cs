﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using TMPro;

public class IntroduceProject : MonoBehaviour
{
    [SerializeField] Image sceneOneBackground;
    [SerializeField] Image trongDong;
    [SerializeField] Image VietNamMap;
    [SerializeField] Image logo;
    [SerializeField] TMP_Text description;
    [SerializeField] float sceneOneTime = 1f;
 
    // Start is called before the first frame update
    void Start()
    {
        SceneOne();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void SceneOne()
    {
        VietNamMap.enabled = true;
        trongDong.enabled = true;
        description.gameObject.SetActive(true);

        AudioManager.instance.Play("DongMauLacHong");
        Sequence sequence = DOTween.Sequence();
        sequence/*.Append(logo.DOBlendableColor(Color.white, 1f))*/
                .Append(logo.transform.DOScale(2, 4f))
                .Append(logo.transform.DOScale(1, 4f))
                .Append(logo.DOFade(0, sceneOneTime))
                .Join(description.DOFade(0, sceneOneTime))
                .Join(VietNamMap.DOFade(0, sceneOneTime))
                .Join(sceneOneBackground.DOFade(0, sceneOneTime))
                .Join(trongDong.DOFade(0, sceneOneTime));
                /*.Join(curBackground.DOFade(0, sceneOneTime - 1))*/;
    }
}
