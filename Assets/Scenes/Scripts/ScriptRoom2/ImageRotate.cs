﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ImageRotate : MonoBehaviour {
    public void Rotate()
    {
        if (!GameControl.youWin)
            this.transform.Rotate(0f, 0f, 90f);
    }
}
