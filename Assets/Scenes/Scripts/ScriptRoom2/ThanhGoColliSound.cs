﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThanhGoColliSound : MonoBehaviour {
    AudioSource audioData;

	// Use this for initialization
	void Start () {
        audioData = GetComponent<AudioSource>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag == "ThanhGo")
        {
            audioData.Play(0);
        }       
    }
}
