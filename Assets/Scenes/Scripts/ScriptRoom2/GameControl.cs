﻿using UnityEngine;

public class GameControl : MonoBehaviour {

    [SerializeField]
    private Transform[] pictures;

    [SerializeField]
    private GameObject winCanvas;

    public static bool youWin;

    // Use this for initialization
    void Start () {
        restart();
    }
	
	// Update is called once per frame
	void Update () {
        if (Mathf.Abs(pictures[0].rotation.w) == 1f &&
            Mathf.Abs(pictures[1].rotation.w) == 1f &&
            Mathf.Abs(pictures[2].rotation.w) == 1f &&
            Mathf.Abs(pictures[3].rotation.w) == 1f &&
            Mathf.Abs(pictures[4].rotation.w) == 1f &&
            Mathf.Abs(pictures[5].rotation.w) == 1f &&
            Mathf.Abs(pictures[6].rotation.w) == 1f &&
            Mathf.Abs(pictures[7].rotation.w) == 1f &&
            Mathf.Abs(pictures[8].rotation.w) == 1f)
        {
            youWin = true;
            winCanvas.SetActive(true);
        }
	}

    public void restart ()
    {
        winCanvas.SetActive(false);
        youWin = false;

        pictures[0].transform.Rotate(0f, 0f, 90 * Random.Range(-3, 4));
        pictures[1].transform.Rotate(0f, 0f, 90 * Random.Range(-3, 4));
        pictures[2].transform.Rotate(0f, 0f, 90 * Random.Range(-3, 4));
        pictures[3].transform.Rotate(0f, 0f, 90 * Random.Range(-3, 4));
        pictures[4].transform.Rotate(0f, 0f, 90 * Random.Range(-3, 4));
        pictures[5].transform.Rotate(0f, 0f, 90 * Random.Range(-3, 4));
        pictures[6].transform.Rotate(0f, 0f, 90 * Random.Range(-3, 4));
        pictures[7].transform.Rotate(0f, 0f, 90 * Random.Range(-3, 4));
        pictures[8].transform.Rotate(0f, 0f, 90 * Random.Range(-3, 4));
    }
}
