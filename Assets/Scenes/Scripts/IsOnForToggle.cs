﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class IsOnForToggle : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (this.GetComponent<Toggle>().isOn)
        {
            this.GetComponent<Image>().color = Color.cyan;
            
        } else
        {
            this.GetComponent<Image>().color = Color.white;
        }    
    }
}
