﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAtCam : MonoBehaviour
{
    public Transform _target;
    // public GameObject target;

    // public float _speed;
    // private Coroutine LookCoroutine;

    // public void StartRotating()
    // {
    //     if(LookCoroutine != null)
    //     {
    //         StopCoroutine(LookCoroutine);
    //     }

    //     LookCoroutine = StartCoroutine(LookAt());
    // }
    // private IEnumerator LookAt()
    // {
    //     Quaternion lookRotation = Quaternion.LookRotation(target.transform.position - this.transform.position);
    //     float time = 0;
    //     while(time < 1)
    //     {
    //         transform.rotation = Quaternion.Slerp(transform.rotation, lookRotation, time);
    //         time += Time.deltaTime * _speed;
    //         yield return null;
    //     }

    // }
    // // Start is called before the first frame update
    // void Start()
    // {
    
    // }

    // Update is called once per frame
    void Update()
    {
        Vector3 _direction = _target.position - transform.position;
        Quaternion _rotation = Quaternion.LookRotation(_direction);
        transform.rotation = _rotation;
    }
}
