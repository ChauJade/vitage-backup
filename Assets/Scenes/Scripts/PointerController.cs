﻿using System;
using UnityEngine;
using VRTK;

public class PointerController : MonoBehaviour
{
    public VRTK_ControllerEvents controllerEvents;
    private bool touchpadPressed = false;
    private void OnEnable()
    {
        controllerEvents.TouchpadReleased += ControllerEvents_TouchpadPressed;
    }
    
    private void OnDisable()
    {
        controllerEvents.TouchpadReleased -= ControllerEvents_TouchpadPressed;
    }

    private void ControllerEvents_TouchpadPressed(object sender, ControllerInteractionEventArgs e)
    {
        touchpadPressed = !touchpadPressed;
        this.GetComponent<VRTK_Pointer>().enabled = touchpadPressed;
        this.GetComponent<VRTK_UIPointer>().enabled = !touchpadPressed;
        Debug.Log(this.GetComponent<VRTK_Pointer>().enabled);
        Debug.Log(this.GetComponent<VRTK_UIPointer>().enabled);
    }
}
