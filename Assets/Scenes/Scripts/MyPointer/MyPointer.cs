﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VRTK;

public class MyPointer : MonoBehaviour
{
    public float m_DefaultLength = 110.0f;
    public GameObject m_Dot;
    public VRTK_UIPointer UIPointer;
    public VRTK_Pointer Pointer;
    public VRTK_BezierPointerRenderer BezierRender;
    public GameObject HitGO;
    public Color color;
    public GameObject myLine;
    // Start is called before the first frame update

    private void Start()
    {
        
    }
    private void Awake()
    {
            
    }

    // Update is called once per frame
    private void Update()
    {
        UpdateLine();
    }
    private void UpdateLine()
    {
        float targetLength = m_DefaultLength;
        RaycastHit hit = CreateRaycast(targetLength);
        Vector3 endPos = transform.position + transform.forward * targetLength;
        if(hit.collider != null)
        {
            endPos = hit.point;
            if(hit.transform.gameObject.name == HitGO.name)
            {
                insideTarget();
                
                drawLine(this.transform.position, endPos, color);
            }
            else
            {
                outsideTarget();
            }
        } else
        {
            outsideTarget();
        }
        m_Dot.transform.position = endPos;
        
        
    }
    private void insideTarget()
    {
        updateEnbale(true);
        this.m_Dot.SetActive(true);
        
    }
    private void outsideTarget()
    {
        updateEnbale(false);
        this.m_Dot.SetActive(false);
        this.myLine.SetActive(false);
    }
    private void updateEnbale(bool status)
    {
        this.UIPointer.enabled = status;
        this.Pointer.enabled = !status;
        if(this.BezierRender != null)
        {
            this.BezierRender.enabled = !status;
        }
        
    }
    private void drawLine(Vector3 start, Vector3 end, Color color)
    {
        myLine.SetActive(true);
        myLine.transform.position = start;
        //myLine.AddComponent<LineRenderer>();
        LineRenderer lr = myLine.GetComponent<LineRenderer>();
        //lr.material = new Material(Shader.Find("Lightweight Render Pipeline/Lit"));
        //lr.SetColors(color, color);
        lr.SetWidth(0.01f, 0.01f);
        lr.SetPosition(0, start);
        lr.SetPosition(1, end);
    }

    private RaycastHit CreateRaycast(float length)
    {
        RaycastHit _hit;
        Ray ray = new Ray(transform.position, transform.forward);
        Physics.Raycast(ray, out _hit, m_DefaultLength);
        return _hit;
    }
}
