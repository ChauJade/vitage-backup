﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class OnlyOneSelect : MonoBehaviour
{
    private int childcount;
    private int count;
    public Color normal;
    public Color selected;
    // Start is called before the first frame update
    void Start()
    {
        childcount = this.transform.childCount;
        //Debug.Log("ChildCout" + childcount);
        for (int i = 0; i < childcount; i++)
        {
            var button = this.transform.GetChild(i).GetComponent<Button>();
            var k = i;
            button.onClick.AddListener(() =>
            {
                select(k);
            });
        }
    }

    // Update is called once per frame
    void Update()
    {
            
    }
    private void select(int k)
    {
        Debug.Log("in select " + k);
        Debug.Log("ChildCout" + childcount);
        for (int i = 0; i < childcount; ++i)
        {
            this.transform.GetChild(i).GetComponent<Image>().color = normal;
        }
        this.transform.GetChild(k).GetComponent<Image>().color = selected;
    }
}
