﻿using UnityEngine.Audio;
using UnityEngine;

public class ButtonHandle : MonoBehaviour
{
    public void OnChangeVietnamese()
    {
        AudioManager.instance.Stop("IntroEnglish");
        AudioManager.instance.Play("IntroVietNam");
        
    }

    public void OnChangeEnglish()
    {
        AudioManager.instance.Stop("IntroVietNam");
        AudioManager.instance.Play("IntroEnglish");
    }

}
