﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class animationTrigger : MonoBehaviour
{
    public Animator trigg;
    // Start is called before the first frame update
    void Start()
    {
        trigg = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey("p"))
        {
            trigg.Play("CungNuAnimation");  
        }
        if (Input.GetKeyUp("p"))
        {
            trigg.Play("IdleState");
        }
    }
}
