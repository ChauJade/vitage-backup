﻿using UnityEngine.Audio;
using UnityEngine;

using System;

public class AudioManager : MonoBehaviour
{
    // public Sound[] sounds;

    // // Start is called before the first frame update
    // void Start()
    // {
        
    // }

    // // Update is called once per frame
    // void Update()
    // {
        
    // }

    public static AudioManager instance;

    public Sound[] sounds;
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        } 
            
        foreach(Sound s in sounds)
        {
            s.audioSource = gameObject.AddComponent<AudioSource>();
            s.audioSource.clip = s.clip;
            s.audioSource.volume = s.volume;
        }
    }

    /*private void Start()
    {
        Play("DongMauLacHong");
    }*/

    public void Play (string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.audioSource.Play();
    }

    public void Stop(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        s.audioSource.Stop();
    }
}
